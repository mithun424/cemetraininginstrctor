package com.allstate.labs.BasicSpring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BasicSpringApp {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(CarConfigurer.class);
        double result = context.getBean(Car.class).getEngine().getEngineSize();
        double result2 = context.getBean(Car.class).getEngine2().getEngineSize();
        System.out.println(result + " " + result2);
    }
}
