package com.allstate.lambdasdemos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.allstate.entities.Employee;

public class TestCollections {
   public static void main(String[] args) {
       


    List<Employee> employees = new ArrayList<Employee>();
    Employee e1= new Employee(1, "dee", 22, "d:i.ie", 2000);
    Employee e2= new Employee(2, "ann", 22, "d:i.ie", 2000);
    Employee e3= new Employee(3, "sue", 22, "d:i.ie", 2000);
    Employee e4= new Employee(1, "jim", 22, "d:i.ie", 2000);

    employees.add(e1);
    employees.add(e2);
    employees.add(e3);
    employees.add(e3);

    for (Employee employee:employees)
    {

    employee.display();
    }

System.out.println("=======");
    employees.stream()
    .filter(p -> p.getName() == "dee" && p.getAge() >= 18)
    .map(p -> p.getEmail())
    .forEach(email -> System.out.println(email));

    //sorting
    
    Set<Employee> employeesTreeSet = new TreeSet<Employee>((p1,  p2) -> p1.getName().compareTo(p2.getName())  );
    employeesTreeSet.add(e1);
    employeesTreeSet.add(e2);
    employeesTreeSet.add(e3);
    employeesTreeSet.add(e3);
    employeesTreeSet.add(e4);

    for (Employee employee:employeesTreeSet)
    {

    employee.display();
    }




   }
}
