package com.allstate.lambdasdemos;

import java.util.function.Function;
import java.util.function.Predicate;

import com.allstate.entities.Employee;

public class TestLambdas {


    public interface Maths {
        int add(int a, int b);  
    }
    
   public static void main(String[] args) {
       
    GreetingServiceImpl greetingServiceImpl = new GreetingServiceImpl();
    greetingServiceImpl.sayMessage("dee");

        //use lambda
        GreetingService greetingService = (s)->{
        System.out.println("Hello " + s);
        } ;
        greetingService.sayMessage("dee");


        Maths maths = (x,y)->{
            return x+y;
        };

       int ans= maths.add(2, 2);
       System.out.println(ans);


    
    Function<Integer, String> myFunc  = (a) -> {
        System.out.println(a);
        return "finished";
    };

String ans2=myFunc.apply(3);
Predicate<String> onlyReturnsTrueOrFalse = (s) -> true;
onlyReturnsTrueOrFalse.test("t");


        }//main
}
