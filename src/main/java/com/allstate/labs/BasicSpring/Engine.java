package com.allstate.labs.BasicSpring;

public interface Engine {
    double getEngineSize();
    void setEngineSize(double engineSize);
}
