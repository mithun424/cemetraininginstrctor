package com.allstate.labs.BasicSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Car {
    private String make="BMW";
    private String model="3 Series";
    
    @Autowired
    @Qualifier("PetrolEngine")
    private Engine engine;

    @Autowired
    @Qualifier("DieselEngine")
    private Engine engine2;
   
    public Engine getEngine() {
        return engine;
    }
    public void setPetrolEngine(Engine engine) {
        this.engine = engine;
    }
    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public Engine getEngine2() {
        return engine2;
    }

    public void setDieselEngine(Engine engine2) {
        this.engine2 = engine2;
    }
}
   