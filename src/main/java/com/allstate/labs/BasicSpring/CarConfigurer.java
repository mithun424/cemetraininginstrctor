package com.allstate.labs.BasicSpring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.allstate")
public class CarConfigurer {
    
}
